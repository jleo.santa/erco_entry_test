# Libs
from pac3200 import pac3200
import threading
import time
import json

# PAC3200 definitions
cementos_ip = "127.0.0.1"
pinturas_ip = "127.0.0.2"
liquidos_ip = "127.0.0.3"
desechos_ip = "127.0.0.4"

printing_time_s  = 10   # seconds
sampling_time_s = 30    # seconds
file_name = "registros.txt"

# PAC3200 instances
pac3200_cementos = pac3200(ip_address=cementos_ip, area="cementos")
pac3200_pinturas = pac3200(ip_address=pinturas_ip, area="pinturas")
pac3200_liquidos = pac3200(ip_address=liquidos_ip, area="liquidos")
pac3200_desechos = pac3200(ip_address=desechos_ip, area="desechos")

# GLOBAL variables
threads = list()
data_list = list()

# functions
def printJsonObj(json_obj):
    print("Telemetry data")
    print("Area = %s, Time-stamp = %d" %(json_obj["area"], json_obj["timestamp"]))
    
    telemetry = json_obj["telemetry"]
    if(len(telemetry)):
        print("V_L1-N = %dV, V_L2-N = %dV, V_L3-N = %d" %(telemetry["v_l1_n"], telemetry["v_l2_n"], telemetry["v_l3_n"]))
        print("I_L1 = %d, I_L2 = %d, I_L3 = %d" %(telemetry["i_l1"], telemetry["i_l2"], telemetry["i_l3"]))
        print("Cumulative energy imported %d" %(telemetry["cum_energy"]))
        print("PF_L1 = %d, PF_L2 = %d, PF_L3 = %d" %(telemetry["pf_l1"], telemetry["pf_l2"], telemetry["pf_l3"]))

    events = json_obj["events"]
    if(len(events)):
        print("Events data")
        for evt in events.values():
            print(evt)

    print("")

# periodic functions definition
def saveData():
    f = open(file=file_name, mode="a")

    if(pac3200_cementos.getSample()):
        f.write(pac3200_cementos.getValuesJSON() + "\n")

    if(pac3200_pinturas.getSample()):
        f.write(pac3200_pinturas.getValuesJSON() + "\n")

    if(pac3200_liquidos.getSample()):
        f.write(pac3200_liquidos.getValuesJSON() + "\n")

    if(pac3200_desechos.getSample()):
        f.write(pac3200_desechos.getValuesJSON() + "\n")

    f.close()

    t = threading.Timer(sampling_time_s, saveData)
    t.start()
    threads.insert(0, t)

def printData():
    for i in range(4):
        f = open(file=file_name, mode="r")
        lines = f.readlines()
        f.close()
    
        json_str = ""
        if(len(lines)):
            json_str = lines.pop(0)
            f = open(file=file_name, mode="w")
            f.writelines(lines)
            f.close()

        if(json_str != ""):
            json_obj = json.loads(json_str)
            printJsonObj(json_obj)

        time.sleep(1)

    t = threading.Timer(printing_time_s, printData)
    t.start()
    threads.insert(1, t)

def checkInput():
    if(input() == "k"):
        print("Stopping app!")

        for i in range(len(threads)):
            threads[i].cancel()

        exit()
    else:
        t = threading.Timer(1, checkInput)
        t.start()
        threads.insert(2, t)

if(__name__ == "__main__"):
    printData()
    saveData()
    checkInput()
    
    