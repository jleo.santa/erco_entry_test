# ERCO_entry_test

Este proyecto busca simular 4 unidades PAC3200 Siemens que leen 10 variables:

    1. Voltaje L1-N
    2. Voltaje L2-N
    3. Voltaje L3-N
    4. Corriente L1
    5. Corriente L2
    6. Corriente L3
    7. Energía acumulada
    8. Factor de potencia L1
    9. Factor de potencia L2    
    10. Factor de potencia L3

La simulación contempla el protocolo MODBUS TCP.

## Consideraciones
    - Se debe instalar la librería pyModbusTCP.
    - Se debe tener instalado el software mbslave.

## Puesta en marcha
Se deben abrir en 4 instancias diferentes cada uno de los siguientes archivos que simulan las unidades PAC3200, utilizando el software mbslave:

    1. PAC3200_cementos.mbs (ip 127.0.0.1)
    2. PAC3200_pinturas.mbs (ip 127.0.0.2)
    3. PAC3200_liquidos.mbs (ip 127.0.0.3)
    4. PAC3200_desechos.mbs (ip 127.0.0.4)

Una vez las 4 instancias estén conectadas y corriendo con las ips que se detallan anteriormente, se debe ejecutar el archivo main.py desde la consola.

## Archivos
    - ERCO_Test_diagram.jpg contiene el diagrama de conexión eléctrica y de comunicación de la solución planteada.
    - pac3200.py contiene la clase pac3200, la cual contiene como propiedad a cada variable que se lee del dispositivo.
    - PAC3200_cementos.mbs contiene la simulación para el área de cementos.
    - PAC3200_pinturas.mbs contiene la simulación para el área de pinturas.
    - PAC3200_liquidos.mbs contiene la simulación para el área de líquidos.
    - PAC3200_desechos.mbs contiene la simulación para el área de desechos.
    - main.py es el archivo a ejecutarse desde la consola.    
    - registros.txt contiene los datos que se guardan y no se han impreso en pantalla.

