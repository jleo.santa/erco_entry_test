import json
from enum import Enum
from pyModbusTCP.client import ModbusClient
import time

class pac_variables(Enum):
    v_l1_n      = 0     # L1-N voltage
    v_l2_n      = 1     # L2-N voltage
    v_l3_n      = 2     # L3-N voltage

    i_l1        = 3     # L1 current
    i_l2        = 4     # L2 current
    i_l3        = 5     # L3 current

    cum_energy  = 6     # cumulated energy demanded

    pf_l1       = 7     # L1 power factor
    pf_l2       = 8     # L2 power factor
    pf_l3       = 9     # L3 power factor

class pac3200:
    def __init__(self, ip_address, area):
        self._ip_address = ip_address

        self._v_l1_n = 0
        self._v_l2_n = 0
        self._v_l3_n = 0

        self._i_l1 = 0
        self._i_l2 = 0
        self._i_l3 = 0

        self._cum_energy = 0

        self._pf_l1 = 0
        self._pf_l2 = 0
        self._pf_l3 = 0

        self._time_stamp = 0
        self._area = area
        self._events = {}
        self._pac = ModbusClient(host=ip_address, auto_open=True, auto_close=True)

    def getSample(self):
        b_return = False

        values = self._pac.read_holding_registers(0, 10)
        
        if(values):
            #update timestamp
            self._time_stamp = time.time()

            # uptade voltages
            self._v_l1_n = values[0]
            self._v_l2_n = values[1]
            self._v_l3_n = values[2]

            # update currents
            self._i_l1 = values[3]
            if(self._i_l1 > 90):
                self._events["L1 alarm"] = "Actual L1 current is over the limit. L1 current =  " + str(self._i_l1) + "A"

            self._i_l2 = values[4]
            if(self._i_l2 > 90):
                self._events["L2 alarm"] = "Actual L2 current is over the limit. L2 current =  " + str(self._i_l2) + "A"

            self._i_l3 = values[5]
            if(self._i_l3 > 90):
                self._events["L3 alarm"] = "Actual L3 current is over the limit. L3 current =  " + str(self._i_l3) + "A"

            # update cumulative energy
            self._cum_energy = values[6]

            # update power factor
            self._pf_l1 = values[7]
            self._pf_l2 = values[8]
            self._pf_l3 = values[9]

            #print("Data saved succesfully (area %s, device %s)!" %(self._area, self._ip_address))
            b_return = True
        else:
            #print("No data available (area %s, device %s)!" %(self._area, self._ip_address))
            b_return = False
            
        return b_return

    def getValuesJSON(self):
        telemetry = {
            pac_variables.v_l1_n.name : self._v_l1_n,
            pac_variables.v_l2_n.name : self._v_l2_n,
            pac_variables.v_l3_n.name : self._v_l3_n,
            
            pac_variables.i_l1.name : self._i_l1,
            pac_variables.i_l2.name : self._i_l2,
            pac_variables.i_l3.name : self._i_l3,
            
            pac_variables.cum_energy.name : self._cum_energy,
            
            pac_variables.pf_l1.name : self._pf_l1,
            pac_variables.pf_l2.name : self._pf_l2,
            pac_variables.pf_l3.name : self._pf_l3,
        }

        data = {
            "area" : self._area,
            "timestamp" : int(self._time_stamp),
            "telemetry" : telemetry,
            "events" : self._events
        }

        return json.dumps(data)
        